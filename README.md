# repex

a minimalistic (one bash file) framework for reproducible experiments in HPC and maybe machine learning?

## What it does...
1. create a folder for your experiment run (so you will be able to run multiple experiments in parallel!)
2. copy input files in this folder
3. saves git repositories and other software dependencies as well as environment variables, kernel version and more...
4. creates started.json with time stamp
5. runs your experiments
6. (TODO: zips the inputs and outputs)
7. creates finished.json with time stamp

## Install
Add this direcory to your `$PATH` - e.g. in your .bashrc .

To use the python interface:
Add this direcory to your `$PYTHONPATH` - e.g. in your .bashrc .


## How To Use
### Bash
- copy the run-example.sh to your experiments input directory and change its name content appropriately.
- launch your experiment by ./run-....sh
### Python
- copy run-example-repex.py to your experiments input directory and change its name and content appropriately.
- launch your experiment by python3 run-example-repex.py



## Testing
- checking has to be done by hand.
- run once:
```bash
mkdir test-folder
touch test-folder/a
touch test
cat <<EOF > CMakeCache.txt
//Path to a program.
CMAKE_OBJDUMP:FILEPATH=/usr/bin/objdump

//Value Computed by CMake
CMAKE_PROJECT_NAME:STATIC=blabla

//Path to a program.
CMAKE_RANLIB:FILEPATH=/usr/bin/ranlib

EOF
```

- to test run `./run-example-repex.sh`
- to test the python interface: `python3 run-example-repex.py`


- **Check by hand the results of the created experiments/ directory!**



## TODOS:
- unify code. bash version should call python version...
